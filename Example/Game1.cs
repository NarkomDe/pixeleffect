﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Example
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        //Main game component
        PixelEffect.PixelManager pixelManager;

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
        }

        protected override void Initialize()
        {
            //Creating blank
            pixelManager = new PixelEffect.PixelManager(this);
            Components.Add(pixelManager);

            base.Initialize();
        }

        protected override void LoadContent()
        {
            //Adding text with XML file and font
            pixelManager.addXMLText("XMLFile1", "SpriteFont1");
            pixelManager.addXMLText("XMLFile2", "SpriteFont1");
            pixelManager.addXMLText("XMLFile3", "SpriteFont2");
            
            spriteBatch = new SpriteBatch(GraphicsDevice);
        }

        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            base.Draw(gameTime);
        }
    }
}
